# GRR: Go Repo Replacer
Replaces all occurrences of a dependency path in go import statements with a new path.

## Usage

(After cloning/go get-ing:)

```
cd go-repo-replacer
go build cmd/grr/grr.go
```

The most simple usage will recurse through the current working directory and replace matching dependencies:
```
./grr oldrepo.com/user newrepo.com/newuser
```

To replace dependencies in a single file:

```
./grr oldrepo.com/user newrepo.com/newuser file.go
```

Or to recursively replace dependencies in a given path:

```
./grr oldrepo.com/user newrepo.com/newuser /given/path
```

## TODO
Add flags for:
- Current directory only (no recursion)
- Interactive: ask at each match
  - Possibly use [AlecAivazis/survey](https://github.com/AlecAivazis/survey) package for interface
- Ignore folders/files
  - For example, vendor folder)
- Verbose/quiet
- List dependencies
  - Report all dependencies in all import statements in files processed (read-only mode)
