package main

import (
	"log"
	"os"
	"sort"
	"strings"
	"time"

	grr "gitlab.com/waveletlet/go-repo-replacer"
	"gopkg.in/urfave/cli.v1"
)

func processIgnoreOptions(ignoreSlice []string, commandLine string) []string {
	ignoreSlice = append(ignoreSlice, ".git")
	ignoreSlice = append(ignoreSlice, strings.Split(commandLine, ",")...)
	sort.Strings(ignoreSlice)
	return ignoreSlice
}

func main() {
	var toIgnore []string

	// Set up cli
	cmd := cli.NewApp()
	cmd.Name = "grr"
	cmd.Usage = "Go Repo Replacer"

	cmd.Flags = []cli.Flag{
		cli.StringFlag{
			// TODO getting "flag provided but not defined" error, what step did I skip here?
			Name:  "ignore, i",
			Usage: "Ignores a file or directory. Accepts comma separated lists.",
		},
	}

	cmd.Action = func(c *cli.Context) error {
		// TODO check c.Args() for sane arguments
		toIgnore = processIgnoreOptions(toIgnore, c.String("ignore"))
		originalRepo := c.Args().Get(0)
		newRepo := c.Args().Get(1)
		path, _ := os.Getwd()
		if c.Args().Get(2) != "" {
			path = c.Args().Get(2)
		}

		grr.ReplaceRepos(originalRepo, newRepo, path, toIgnore)

		return nil
	}

	err := cmd.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}

func benchmark(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s to exe", name, elapsed)
}
