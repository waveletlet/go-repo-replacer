package grr

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

func ReplaceRepos(origRepo string, newRepo string, rootpath string, toIgnore []string) (err error) {
	err = filepath.Walk(rootpath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("Problem traversing path %q: %v/n", path, err)
			return err
		}

		switch {
		case info.IsDir() && inWordList(toIgnore, info.Name()):
			return filepath.SkipDir
		case info.IsDir() && !inWordList(toIgnore, info.Name()):
			return nil
		case filepath.Ext(path) != ".go":
			return nil
		case inWordList(toIgnore, info.Name()):
			return nil
		case !inWordList(toIgnore, info.Name()):
			// do the replacing
			infile, err := ioutil.ReadFile(path)
			if err != nil {
				fmt.Println(err)
				return err
			} else {
				regexed := editImportStatement(infile, origRepo, newRepo)
				err = ioutil.WriteFile(path, regexed, info.Mode())
				if err != nil {
					fmt.Println(err)
					return err
				}
				fmt.Printf("Replaced '%s' with '%s' in \t\t'%s'\n", origRepo, newRepo, path)
				return nil
			}
		default:
			err = fmt.Errorf("Failed to classify %s or %s", path, info.Name())
			fmt.Printf("Error: %s\n", err)
			return err
		}
		return nil
	})

	if err != nil {
		fmt.Println(err)
	}
	return
}

func inWordList(slice []string, word string) bool {
	for index := 0; index < len(slice); index++ {
		if word == slice[index] {
			return true
		}
	}
	return false
}

func editImportStatement(fileBytes []byte, origRepo string, newRepo string) []byte {
	// Find and replace in import statement
	importPattern := regexp.MustCompile(`import\s("[^"]+"|\([^)]+\))`)
	importStatement := importPattern.Find(fileBytes)
	origRepoRE := regexp.MustCompile(origRepo)
	newImportStatement := origRepoRE.ReplaceAllString(string(importStatement), newRepo)

	// Join new import statement with old body
	splitted := importPattern.Split(string(fileBytes), 2)
	joined := strings.Join(splitted, newImportStatement)
	return []byte(joined)
}
