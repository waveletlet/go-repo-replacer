package grr

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestReplaceRepos(t *testing.T) {
	PrepTestFile()
	path = "mockfile.go"

	result := ReplaceRepos("str1", "str2")
	if result != nil {
		t.Fail()
	}
}

// Use the following as a better way to rework this:
// func prepareTestDirTree(tree string) (string, error) {
//	tmpDir, err := ioutil.TempDir("", "")
//	if err != nil {
//		return "", fmt.Errorf("error creating temp directory: %v\n", err)
//	}
//
//	err = os.MkdirAll(filepath.Join(tmpDir, tree), 0755)
//	if err != nil {
//		os.RemoveAll(tmpDir)
//		return "", err
//	}
//
//	return tmpDir, nil
//}
// func main() {
//	tmpDir, err := prepareTestDirTree("dir/to/walk/skip")
//	if err != nil {
//		fmt.Printf("unable to create test dir tree: %v\n", err)
//		return
//	}
//	defer os.RemoveAll(tmpDir)
//	os.Chdir(tmpDir)
//
//	subDirToSkip := "skip"
//  //...
//  }

func PrepTestFile() {
	masterFile, err := ioutil.ReadFile("../../mockfile.go.master")
	err = ioutil.WriteFile("mockfile.go", masterFile, 0644)
	if err != nil {
		fmt.Println(err)
	}
	return
}

// Example of testing
//func Sum(x int, y int) int {
//    return x + y
//	}
//
//func TestSum(t *testing.T) {
//    total := Sum(5, 5)
//    if total != 10 {
//       t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 10)
//    }
//	}
